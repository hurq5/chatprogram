package com.example.chatproject;

import com.alibaba.fastjson.JSONObject;
import com.example.chatproject.mapper.Messages;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.websocket.Session;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@ServerEndpoint(value = "/websocket/{option}")
@Component
public class SocketController {
    private static Messages tempMessages;
    @Autowired
    public void setTempMessages(Messages tempMessages) {
        SocketController.tempMessages = tempMessages;
    }
    public class SocketUserInfo {
        private String sessionId;
        private Session session;

        //get方法和set方法
        public String getSessionId() {
            return sessionId;
        }

        public void setSessionId(String sessionId) {
            this.sessionId = sessionId;
        }

        public Session getSession() {
            return session;
        }

        public void setSession(Session session) {
            this.session = session;
        }

    }
    final int MSG_LIB_SIZE=10;
    String[] msgLib={"hello","hi","Are you ok","Thank you","Hello", "Good morning","Good afternoon" ,"I’m sorry.","I couldn’t.","I can."};
    //记录所有连接上的客户端的会话，模拟浏览器记录用户数据
    private static Map<String, SocketUserInfo> userSessionMap = new ConcurrentHashMap<>();

    //得到随机消息的方法
//    String getRandomMsg(){
//        java.util.Random r=new java.util.Random();
//        //System.out.println(r.nextInt());
//        return msgLib[Math.abs(r.nextInt())%MSG_LIB_SIZE];
//    }


    String getRandomMsgFromFile() throws IOException {
        FileReader fr=new FileReader("C:\\Users\\you\\IdeaProjects\\chatproject\\src\\main\\resources\\msg.txt");
        BufferedReader br=new BufferedReader(fr);
        String line="";
        String[] arrs=null;
        java.util.Random r=new java.util.Random();
        int RandomNum=Math.abs(r.nextInt())%MSG_LIB_SIZE;
        String result = null;
        if((line=br.readLine())!=null) {
            arrs=line.split(",");
            result=arrs[RandomNum];
        }
        br.close();
        fr.close();
        return result;
    }

    String getRandomMsgFromDatabase(){
        java.util.Random r=new java.util.Random();
        int RandomNum=Math.abs(r.nextInt())%MSG_LIB_SIZE;
        return tempMessages.selectOne(RandomNum);
    }

    String getRandomMsgFromAPI(String url) throws IOException, URISyntaxException {
        //通过get方式获得
        // 使用帮助类HttpClients创建CloseableHttpClient对象
        CloseableHttpClient httpclient = HttpClients.createDefault();
        CloseableHttpResponse response=null;
        // 创建uri
        URIBuilder builder = new URIBuilder(url);
        URI uri = builder.build();
        // 创建http GET请求实例
        HttpGet httpGet = new HttpGet(uri);
        // 执行请求
        response = httpclient.execute(httpGet);
        // 判断返回状态是否为200，即是否响应正常
        String result = "";
        if (response.getStatusLine().getStatusCode() == 200) {
            result = EntityUtils.toString(response.getEntity(), "UTF-8");
        }
        if (response != null) {
            response.close();
        }
        httpclient.close();
        return result;
    }
    //发送消息的方法
    private synchronized void sendMsg(Session session, String msg) {
        try {
            session.getBasicRemote().sendText(msg);//阻塞型发送
        } catch (IOException e) {
            e.printStackTrace();//在命令行打印异常信息在程序中出错的位置及原因
        }
    }

    //连接
    //创建一个在线客户端会话信息
    @OnOpen
    public void onOpen(Session session) {
        SocketUserInfo userInfo = new SocketUserInfo();
        userInfo.setSessionId(session.getId());
        userInfo.setSession(session);
        // 使用帮助类HttpClients创建CloseableHttpClient对象
        CloseableHttpClient httpclient = HttpClients.createDefault();
        CloseableHttpResponse response=null;
        //告诉客户端连接成功
        //将在线客户端信息保存到map中
        userSessionMap.put(session.getId(), userInfo);
        System.out.println("会话" + session.getId() + "连接成功");

        //返回连接信息
        System.out.println("Connect Success");

        sendMsg(session, "Connect Success");
    }

    //关闭连接
    @OnClose
    public void onClose(Session session) {
        //会话关闭，客户端不能够继续和服务端通讯
        //将会话从map中移除
        userSessionMap.remove(session.getId());
        System.out.println("会话" + session.getId() + "连接断开");
    }

    //客户端和服务端互相传递消息
    @OnMessage
    public void onMessage(String message, Session session, @PathParam("option")String option) throws IOException, URISyntaxException {
        //消息
        if(option.equals("FromFile")){
            String result=getRandomMsgFromFile();
            sendMsg(session, result);
        }
        else if(option.equals("FromDatabase")){
            String result=getRandomMsgFromDatabase();
            sendMsg(session, result);
        }
        else if(option.equals("FromAPI")) {
            String result=getRandomMsgFromAPI("http://192.168.0.203:8000/chat?text=%E6%88%91%E6%98%AF%E8%B0%81");
            sendMsg(session, result);
        }
        else sendMsg(session, "没有指定消息来源");
    }

    //异常
    @OnError
    public void onError(Session session, Throwable throwable) {
        System.out.println("出现异常");
        throwable.printStackTrace();//在命令行打印异常信息在程序中出错的位置及原因
    }

}

