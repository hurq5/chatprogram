package com.example.chatproject.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
@Mapper
public interface Messages {
    @Select("select message from messages where id = #{id}")
    public String selectOne(@Param("id") int id);
}
