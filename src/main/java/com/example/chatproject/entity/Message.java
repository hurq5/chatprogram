package com.example.chatproject.entity;

/**
 * @Description: TODO
 * @author: Ruoqing Hu
 * @date: 2021年08月05日 14:53
 */
public class Message {
    private long id;

    private String message;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
